# Project: comics
# 
# Author: Aditya Bhagwat
###############################################################################

# Make factor levels unique by adding spaces
uniquify <- function(avector){
   while(any(duplicated(avector))){
      idxDuplicates <- which(duplicated(avector))
      avector[idxDuplicates] <- paste0(avector[idxDuplicates], ' ')
   }
   return(avector)
}

utils::globalVariables('tmp_sample_id')
utils::globalVariables('abundances')
utils::globalVariables('feature_id')
utils::globalVariables('fvarname')
#' Plot feature profiles
#' @param esetObj eSet
#' @param x pData variable mapped to x
#' @param colorVar pData variable mapped to color
#' @param shapeVar pData variable mapped to shape
#' @param fvar fData variable that will be used to annotate the plot
#' @param title plot title
#' @param ylab ylab 
#' @return ggplot object
#' @author Aditya Bhagwat
#' @importFrom Biobase fvarLabels
#' @importFrom Biobase fData
#' @importFrom Biobase featureNames
#' @importFrom Biobase pData<-
#' @importFrom Biobase sampleNames
#' @importFrom ggplot2 ggplot
#' @importFrom ggplot2 aes_string
#' @importFrom ggplot2 geom_point
#' @importFrom ggplot2 facet_wrap
#' @importFrom ggplot2 theme_bw
#' @importFrom ggplot2 theme
#' @importFrom ggplot2 ggtitle
#' @importFrom ggplot2 ylab
#' @importFrom ggplot2 element_text
#' @importFrom magrittr %>%
#' @importFrom tidyr gather
#' @export
#' @examples
#' require(ALL)
#' data(ALL)
#' comics::plotFeatureProfiles(ALL[1:10, ], color = 'sex')
plotFeatureProfiles <- function(esetObj, x = NULL, colorVar = NULL, shapeVar = NULL, fvar = NULL, title = '', ylab = 'log2 abundances'){

   # Defaults
   if (is.null(fvar)){
     Biobase::fData(esetObj)$feature_id <- Biobase::featureNames(esetObj)
     fvar <- 'feature_id'
   }
  
   # Checks
   assert_is_valid_eset(esetObj)
   if (!(fvar %in% Biobase::fvarLabels(esetObj))){
     stop(sprintf("plotFeatureProfiles: fvar '%s' not in fvarLabels(esetObj)", fvar))
   }
   if(any(is.na(Biobase::fData(esetObj)[[fvar]]))){
      stop('plotFeatureProfiles: make sure that the values for fvar do not contain any NAs')
   }
   # More defaults
   if (is.null(x)){
      esetObj$sample_id <- Biobase::sampleNames(esetObj)
      x = 'sample_id'
   }
   
   fvarvalues <- Biobase::fData(esetObj)[[fvar]] %>% uniquify() %>% factorify()

   plotdf <- as.data.frame(Biobase::exprs(esetObj))
   plotdf <- cbind(feature_id = rownames(plotdf), fvarname = fvarvalues, plotdf, stringsAsFactors = FALSE)
   plotdf <- tidyr::gather(plotdf, tmp_sample_id, abundances, -feature_id, -fvarname)
   
   Biobase::pData(esetObj)$tmp_sample_id <- Biobase::sampleNames(esetObj)
   plotdf <- merge(plotdf, Biobase::pData(esetObj), by = 'tmp_sample_id')
   
   p <- ggplot(plotdf, aes_string(x = x, y = 'abundances', color = colorVar, shape = shapeVar)) + 
        geom_point(na.rm = TRUE) + 
        facet_wrap(~ fvarname, scales = 'free_y') +
        theme_bw() +
        theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1)) +
        ggtitle(title) + ylab(ylab)
   p
}

