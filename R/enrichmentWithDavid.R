# Project: comics
# 
# Author: Aditya Bhagwat
###############################################################################

# #' Perform an enrichment analysis using DAVID
# #' @param query query genes (entrez ids or gene symbols)
# #' @param universe universe genes (entrez ids or gene symbols)
# #' @param email an email registered with DAVID
# #' @param file_name file to which results will be written
# #' @param cluster_annotations should david return a clustered annotation result?
# #' @param categories annotation categories to include. Interesting choices are:
# #'        'GOTERM_BP_FAT', 'GOTERM_MF_FAT', 'KEGG_PATHWAY', 'SP_PIR_KEYWORDS', 'INTERPRO'
# #'        Complete list: \url{http://david.abcc.ncifcrf.gov/content.jsp?file=DAVID_API.html#approved_list}
# #' @param verbose whether to report information on progress
# #' @author Aditya Bhagwat
# #' @importFrom RDAVIDWebService DAVIDWebService
# #' @importFrom RDAVIDWebService addList
# #' @importFrom RDAVIDWebService getClusterReportFile
# #' @importFrom RDAVIDWebService getFunctionalAnnotationChartFile
# #' @importFrom RDAVIDWebService setAnnotationCategories
# #' @export
# david_enrichment <- function(query, email, file_name, universe = NULL, cluster_annotations = TRUE,   
#       categories = c('GOTERM_BP_FAT', 'GOTERM_MF_FAT', 'KEGG_PATHWAY', 'SP_PIR_KEYWORDS', 'INTERPRO'), 
#       verbose = TRUE
# ){
#    
#    # If gene symbols supplied, convert to entrez IDs 
#    if (verbose) message('\n')
#    if (!all(grepl('^\\d+$', query))){
#       query <- symbol2entrezg(query)
#       if (verbose)
#          message("rm genes that couldn't be mapped to entrez ids: ", 
#                paste0(query[is.na(query)], collapse = ', '))
#    }
#    if (!is.null(universe))
#       if(!all(grepl('^\\d+$', query)))      
#          universe <- symbol2entrezg(universe)
#    
#    # Connect with DAVID
#    if (verbose) message('Initializing DAVIDWebService');    #.jinit()
#    david <- RDAVIDWebService::DAVIDWebService$new(email = email)
#    
#    # Add query and universe gene lists
#    if (verbose) 
#       message('Adding a gene list')
#    result <- RDAVIDWebService::addList(david, query, idType = 'ENTREZ_GENE_ID', listName = 'my_query', listType = 'Gene')
#    if (!is.null(universe))
#       result <- RDAVIDWebService::addList(david, universe, idType = 'ENTREZ_GENE_ID', listName = 'my_universe', listType = 'Background')
#    
#    # Set annotation categories
#    if (verbose){message('Setting the annotation categories')}
#    RDAVIDWebService::setAnnotationCategories(david, categories)
#    
#    # Run enrichment analysis
#    if (verbose){message('Requesting the functional enrichment analysis')}
#    if (cluster_annotations){
#       RDAVIDWebService::getClusterReportFile(david, file_name)
#    } else {
#       RDAVIDWebService::getFunctionalAnnotationChartFile(david, file_name)
#    }
# }

