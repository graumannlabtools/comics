# Project: comics
# Annotation functions
# Author: Aditya Bhagwat
###############################################################################

#' Convert UniProt IDs to EntrezGene IDs
#' 
#' Converts a set of UniProt IDs to EntrezGene IDs.
#' @param uniprot_set A character vector of UniProt IDs.
#' @param organism A string naming the organism that the IDs relate to.
#' @param verbose  Logical. If \code{TRUE}, more output is provided.
#' @importFrom AnnotationDbi select
#' @importFrom magrittr %>% extract2
#' @importFrom org.Hs.eg.db org.Hs.eg.db
#' @export
uniprot_set_to_entrezg_set <- function(uniprot_set, organism = c("human", "mouse", "frog"), verbose = TRUE){
  organism <- match.arg(organism)
  annmap <- switch(
    organism, 
    human = org.Hs.eg.db::org.Hs.eg.db,
    mouse = {
      installPackageIfNecessary("org.Mm.eg.db")
      org.Mm.eg.db::org.Mm.eg.db
    },
    frog  = {
      installPackageIfNecessary("org.Xl.eg.db")
      org.Xl.eg.db::org.Xl.eg.db
    }
  )
  
  # Select uniprot accesions with entrezg mappings
  selector    <- uniprot_set %in% keys(annmap, 'UNIPROT')
  uniprot_set <- uniprot_set[selector]
  
  # Map to entrezg_ids and return
  entrezg_ids <- suppressWarnings(
    AnnotationDbi::select(annmap, uniprot_set, "ENTREZID", "UNIPROT")
  ) %>%
    extract2('ENTREZID') %>%
    unique()
  entrezg_ids
}

#' @noRd
#' @examples
#' feature_set_id_to_name('hsa00010',                    'KEGGPATHWAYS')
#' feature_set_id_to_name('GO:0044848',                  'GOBP'        )
#' feature_set_id_to_name(c('GO:0044848', 'GO:0006915'), 'GOBP'        )
feature_set_id_to_name <- function(id, collection, organism = 'human'){
  assert_is_annotated_organism(organism)
  assert_all_are_feature_set_collections(collection)
  if (collection %in% c('GOBP', 'GOMF', 'GOCC')){  go2term(id)
  } else if (collection == 'KEGGPATHWAYS'){        kegg_pathway_id_to_name(id, organism)
  } else if (collection == 'UNIPROTKEYWORDS'){     id
  }
}

#' Map GO ID to corresponding Term (short) or Definition (long)
#' @usage
#'    go2term(refId)
#'    go2definition(refId)
#' @param refId GO ID
#' @return GO Definition
#' @importFrom AnnotationDbi get
#' @importFrom GO.db GOTERM
#' @examples
#'   require(comics)
#'   go2term('GO:0000002')
#'   go2definition('GO:0000002')
#' @export
go2definition <- function(refId){
  AnnotationDbi::get(refId, GO.db::GOTERM)@Definition
}

#' Map GO ID to constituent uniprot IDs 
#' @param GOID GO identifier, e.g. 'GO:0030168'
#' @return vector with uniprot IDs
#' @author Aditya Bhagwat
#' @importFrom org.Hs.eg.db org.Hs.egGO2ALLEGS org.Hs.egUNIPROT
#' @importFrom AnnotationDbi get mget
#' @export
go2uniprot <- function(GOID){
  entrezIds <- AnnotationDbi::get(GOID, org.Hs.egGO2ALLEGS)
  uniprotIds <- AnnotationDbi::mget(entrezIds, org.Hs.egUNIPROT)
  uniprotIds <- unique(unlist(uniprotIds, use.names = FALSE))
  uniprotIds <- subset(uniprotIds, uniprotIds != 'NA')
  return(uniprotIds)
}

#' @describeIn go2definition
#' @importFrom AnnotationDbi mget
#' @importFrom GO.db GOTERM
#' @importFrom magrittr %>%
#' @export
go2term <- function(refId){
  AnnotationDbi::mget(refId, GO.db::GOTERM) %>%
    sapply('getElement', 'Term') %>%
    unname()
}

#' @noRd
#' @importFrom magrittr %>%
#' @examples 
#' kegg_pathway_id_to_name('hsa00010')
#' kegg_pathway_id_to_name('1234')
kegg_pathway_id_to_name <- function(pathwayId, organism){
  assert_all_are_kegg_pathway_ids(pathwayId, organism)
  assert_is_annotated_organism(organism)
  load_kegg_pathways_to_names(organism) %>% extract(pathwayId) %>% 
    unname()
}

#' Convert gene symbols to entrez ids
#' 
#' Converts a vector with gene symbols to a vector with entrez ids.
#' The output vector contains NAs for corresponding NAs in the input vector
#' or when no mapping is available for that feature.
#' 
#' @param gene_symbols a list with gene symbols 
#' @return list with entrez gene ids
#' @author Aditya Bhagwat
#' @importFrom AnnotationDbi ls mget
#' @importFrom org.Hs.eg.db org.Hs.egSYMBOL2EG
#' @export
symbol2entrezg <- function(gene_symbols){
   entrez_ids <- rep(NA, length(gene_symbols))
   selector <- gene_symbols %in% AnnotationDbi::ls(org.Hs.egSYMBOL2EG)
   entrez_ids[selector] <- lapply(
         mget(gene_symbols[selector], org.Hs.egSYMBOL2EG), 
         getElement, 
         1
   )
   unlist(entrez_ids)
}

#' Convert entrez ids to gene symbols
#' 
#' Converts a vector with entrez ids to a vector with gene symbols.
#' The output vector contains NAs for corresponding NAs in the input vector
#' or when no mapping is available for that feature.
#' 
#' @param entrez_ids vector with entrez ids
#' @return vector with gene symbols
#' @author Aditya Bhagwat
#' @importFrom AnnotationDbi ls mget
#' @importFrom org.Hs.eg.db org.Hs.egSYMBOL
#' @export
entrezg2symbol <- function(entrez_ids){
   gene_symbols <- rep(NA, length(entrez_ids))
   selector <- entrez_ids %in% AnnotationDbi::ls(org.Hs.egSYMBOL)
   gene_symbols[selector] <- lapply(
         mget(entrez_ids[selector], org.Hs.egSYMBOL), 
         getElement, 
         1
   )
   unlist(gene_symbols)
}