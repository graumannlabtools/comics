# This probably needs to go in a separate utilities package at some point.
#' @importFrom utils install.packages
installPackageIfNecessary <- function(pkg)
{
  if(!requireNamespace(pkg, quietly = TRUE))
  {
    warning("The package ", pkg, "is not available; trying to install it.")
    oldOps <- options(warn = 2)
    on.exit(options(oldOps))
    install.packages(pkg)
  }
}

# Copied & pasted from devtools
rule <- function (..., pad = "-") 
{
  if (nargs() == 0) {
    title <- ""
  } else {
    title <- paste0(...)
  }
  width <- getOption("width") - nchar(title) - 1
  message(title, " ", paste(rep(pad, width, collapse = "")))
}

#' Factorify a vector in an order-preserving way
#' @param aVector (non factor) vector
#' @noRd
factorify <- function(aVector){
  factor(aVector, levels = unique(aVector))
}