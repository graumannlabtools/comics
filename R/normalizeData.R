#' Calculate mode of a vector
#' @importFrom stats density
#' @importFrom magrittr %>%
#' @noRd
calcMode <- function(aVector){
  res <- aVector %>% density(na.rm = TRUE)
  res$x[which.max(res$y)]
}

#' Mode center a vector
#' @noRd
modeCenter <- function(aVector){
  aVector - calcMode(aVector)
}

#' Mode center each sample in the eset
#' @param exprSet ExpressionSet
#' @importFrom magrittr %<>%
#' @export
modeCenterSamples <- function(exprSet){
  exprs(exprSet) %<>% apply(2, modeCenter)
  exprSet
}

#' @export
rankNormalize <- function(x, ...){
  UseMethod("rankNormalize")
}

#' Rank normalization
#'
#' Take ranks and use inverse normal distribution to create normally distributed
#' data with the same rank as the input.  Sometimes known as 'quantile
#' normalization'.
#' @param x A numeric vector.
#' @return A normally distributed numeric vector.
#' @examples
#' x <- runif(1e3)
#' normalized <- rankNormalize(x)
#'
#' # The result is normally distributed
#' plot(density(normalized))
#' qqnorm(normalized)
#'
#' # The rank is the same as the input
#' identical(rank(x), rank(normalized))
#' @noRd
#' @importFrom stats qnorm
rankNormalize.numeric <- function(x){
  # This is identical to the ?pROC::var.roc help page (see 'Binormality Assumption' section).
  # Slight variants exist, e.g. Vincent Zoonekynd uses
  # qnorm((rank(x) - 0.5) / length(x))
  # here: http://zoonek2.free.fr/UNIX/48_R/05.html
  # which gives subtly different values
   y <- x
   selector <- !is.na(x) & !is.nan(x)
   y[selector] <- qnorm(rank(x[selector]) / (length(x[selector]) + 1))
   y
}

#' @importFrom magrittr %<>%
#' @importFrom Biobase exprs exprs<-
rankNormalize.eset <- function(x, dim){
  normMat <- Biobase::exprs(x) %>% apply(dim, rankNormalize.numeric)
  if (dim==1){
    normMat %<>% t()
  }
  Biobase::exprs(x) <- normMat
  x
}

#' Rank normalize each sample in the exprSet
#' @param x ExpressionSet
#' @param dim normalize per feature (dim = 1) or per sample (dim = 2)
#' @export
rankNormalize.ExpressionSet <- function(x, dim){
  rankNormalize.eset(x, dim)
}

#' Rank normalize each sample in the exprSet
#' @param exprSet ExpressionSet
#' @importFrom magrittr %<>%
#' @importFrom Biobase exprs exprs<-
#' @export
rankNormalizeSamples <- function(exprSet){
  .Deprecated('rankNormalize.eset')
   Biobase::exprs(exprSet) %<>% apply(2, rankNormalize)
   exprSet
}
