# comics

cornell package for omics data analysis

### Installation

To install the package, you first need the 
[*devtools*](https://github.com/hadley/devtools) package.

```{r}
install.packages("devtools")
```

Then install the following dependencies from CRAN:
```{r}
  source("http://bioconductor.org/biocLite.R")
  biocLite(c('AnnotationDbi', 'Biobase', 'dplyr', 'ggplot2', 'mpm', 'org.Hs.eg.db', 'RDAVIDWebService', 'reshape2', 'rJava'))
```

After that, install the following dependency from the Graumannlab bitbucket account:
```{r}
library(devtools)
install_bitbucket(
  "graumannlab/multipanelfigure",
  auth_user = "your bitbucket username", 
  password  = "your bitbucket password"  
)
```

Finally, you can install the *comics* package using

```{r}
library(devtools)
install_bitbucket(
  "graumannlab/comics",
  auth_user = "your bitbucket username", 
  password  = "your bitbucket password"  
)
```